

cadena: str = "Hola mundo"
#Obtener el tamaño de una cadena
tamanio = len(cadena)
print(tamanio)
#Accedo a la última posición de la cadena
print( cadena[tamanio-1] )

#Rebanamos la cadena y concatenamos con otra
nueva_cadena = "J"+cadena[1:]
print(nueva_cadena)
