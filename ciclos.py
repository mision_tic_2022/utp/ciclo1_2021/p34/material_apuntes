
lista:list = []
#Va a iterar de 0 hasta 99
for x in range(100,10,-2):
    print(x)

"""
Desarrolle un programa que llene una lista con 
elementos del 1 al 400 en saltos de 4, al
momento de almacenar los datos debe de estar elevado
al cuadrado
"""
#Carlos
lista:list=[]
for x in range(1,400,4):
    lista.append(x**2)
print(lista)

#Silvia
lista:list=[]
for x in range(1,400,4):
    lista.append(x**2)
print(x)

print("--------WHILE----------")

numero = 10
while numero > 0:
    numero -= 1
    if numero == 5:
        continue
        #Romper el ciclo (se sale del ciclo actual)
        #break
    print(numero)

print("Fin del while")

print("---------------")

frutas = {
    'fresa':'roja',
    'manzana': 'verde',
    'banano': 'amarillo'
}
"""
for x in frutas.values():
    print( x )
"""

print("-----------EJERCICIO CAJERO------------")

def retiro_dinero(info_retiro: dict):
    retiro = info_retiro["valor_retirar"]
    saldo = info_retiro["saldo_inicial"]
    comision = 2000
    mensaje = ""
    if retiro > 20000:
        if (retiro+comision) <= saldo:
            saldo -= (retiro+comision)
            mensaje = "¡Retiro exitoso!\nSu saldo actual es ${}".format(saldo)
        else:
            mensaje = "El valor a retirar mas la comisión \
                superan su saldo actual"
    elif retiro <= saldo:
        saldo -= retiro
        mensaje = "¡Retiro exitoso!\nSu saldo actual es ${}".format(saldo)
    else:
        mensaje = "El valor a retirar supera su saldo actual"
    return mensaje

#Diccionario con la información del retiro
info_retiro: dict = {
    "valor_retirar": 24000,
    "saldo_inicial": 25000
}
print( retiro_dinero(info_retiro) )

"""
Desarrolle una función que reciba como parametro
un diccionario el cual contiene cinco notas 
de un estudiante.
Retorne el promedio de las notas
"""
print("-----------------------------------------")
def calcular_promedio_notas(notas:dict)->float:
    suma_notas = 0
    for nota in notas.values():
        suma_notas += nota
    
    promedio_notas = suma_notas / len(notas)
    return print("El promedio de notas es {:,.2}".format(promedio_notas))

nostas_estudiantes = {
    'carlos': 4.8,
    'maria': 4.8,
    'julian': 4.8,
    'natalia': 4.8,
    'anahí': 4.8,
    
}

calcular_promedio_notas(nostas_estudiantes)
"""
def prom (notas_estud: dict):
    sumas=0
    for i in notas_estud.values():
        sumas=i+sumas
    promedio=sumas/5
    return (promedio)

notas_estu: dict={}
for x in range(0,5):
    notas_estu[x]=int(input("Por favor ingresar la nota:"))


print( prom(notas_estu) )
"""
def calcular_notas(notas:dict)->float:
    suma_notas= 0
    for x in notas.values():
        suma_notas+=x

    promedio_notas= suma_notas/len(notas)
    return print("El promedio de notas es {} ".format(promedio_notas))

notas_estudiante:dict={
    "nota1":4.8,
    "nota2": 4.8,
    "nota3": 4.8,
    "nota4": 4.8,
    "nota5": 4.8,
}
calcular_notas(notas_estudiante)