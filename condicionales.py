
"""
variable: bool = True
numero_1 = 10
numero_2 = 10
#True -> 1
#False -> 0

if numero_1 == numero_2:
    print("Esto es verdad")
else:
    print("Esto es falso")
"""
"""
 A>B ->A MAYOR QUE B
 A<B ->A MENOR QUE B
 A>=B -> A MAYOR O IGUAL QUE B
 A<= -> A MENOR O IGUAL QUE B
 A==B -> A IGUAL QUE B
"""
"""
Ejercicio 1:
Desarrollar una funcion que reciba la nota de un estudiante e indicarle
si pasó o no el curso. (con una nota MAYOR de 3 se aprueba)
"""
"""
def nota_estudiante(nota: float):
    aprobo: bool = False
    if nota > 3:
        aprobo = True
    else:
        aprobo = False
    return aprobo




estado_curso = nota_estudiante(float(input("Por favor ingrese la nota del estudiante: ")))

if estado_curso == True:
    print("Aprobó la materia")
else:
    print("Reprobó")
"""

# Condicionales en cascada
"""
nota_minima = 3
nota_estudiante = 4.1
if nota_estudiante < nota_minima and nota_estudiante > 1:
    print("En el rango de 1 a 2.9")
elif nota_estudiante >= nota_minima and nota_estudiante < 4:
    print("En el rango de 3 a 3.9")
elif nota_estudiante >= 4:
    print("Nota mayor o igual a 4")
"""

"""
Ejercicio 2:
Desarrollar una funcion que reciba como parametro
el valor de las ventas de 4 días y 
calcular el promedio. 
Si el promedio de ventas es menor a $100.000 
debe de RETORNAR el siguiente mensaje:
-> Se encuentra en un nivel crítico de ventas
"""
# Jose

"""
def promedioVentas(dia1, dia2, dia3, dia4):
    Promedio = (dia1+dia2+dia3+dia4)/4
    mensaje = ""
    if Promedio < 100000:
        mensaje = 'Se encuentra en un nivel critico de ventas'
    else:
        mensaje = 'esta bien de ventas'
    return mensaje


print(promedioVentas(10000, 150000, 500000, 5000))
"""
"""
#Laura
def promedio_ventas(dia1:int, dia2:int, dia3:int, dia4:int):
    suma = dia1 + dia2 + dia3 + dia4
    promedio = suma/4
    if promedio < 100000:
        print("Se encuentra en un nivel crítico de ventas.")
promedio_ventas(2,5,4,1)

#Ricardo
def PromedioVentas(dia1: float, dia2: float, dia3: float, dia4: float) -> str:
    promedio : float = (dia1 + dia2 + dia3 + dia4) / 4
    verdadero :bool = True
    mensaje = ""
    if promedio < 100000:
        mensaje = print("Se encuentra en un nivel critico de ventas")        
            
    return mensaje 


PromedioVentas(12500,3000,4250,11850.23)


#Carlos
def promedio_ventas(dia1,dia2,dia3,dia4):
    promedio= (dia1+dia2+dia3+dia4)/4
    if (promedio < 100):
        return ("se encuentra en un nivel crítico")
    

dia1= 20
dia2= 20
dia3= 20
dia4= 20

print(promedio_ventas(dia1,dia2,dia3,dia4))
"""
"""
def promedio(ventas1:int, ventas2:int, ventas3:int, ventas4:int):
    promedio = (ventas1 + ventas2 + ventas3 +ventas4) / 4
    if promedio < 100000:
        print("Se en cuentra en un nivel critico de ventas")

promedio(10000,20000,30000,40000)
"""
