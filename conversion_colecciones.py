
mi_lista = [ x for x in range(0,10)  ]
print(mi_lista)

#Conversion de lista a conjunto
conjunto = set(mi_lista)
print(conjunto)

#Conversión de lista a tupla
tupla = tuple(mi_lista)
print(tupla)

diccionario = {
    "ciudad1": "Medellín",
    "ciudad2": "Bogotá",
    "ciudad3": "Cali",
    "ciudad4": "Manizales"
}

#Lista con las llaves del diccionario
llaves: list = list(diccionario.keys()) 
print(llaves)
#lista con los valores del diccionario
valores: list = list(diccionario.values())
print(valores)
#Convertir lista a string
print( "".join(valores) )
print("--------------CADENA DE CARACTERES---------------")
hola = "Hola"
#Convertir un string a lista
print( list(hola)[:3] )
#Rebanar una cadena de caracteres
print( hola[0:2] )