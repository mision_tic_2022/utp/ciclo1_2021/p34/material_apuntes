
# Crear un diccionario
# Multivaluado
"""
diccionario:dict = {
    'hola': 'hi',
    'llave': 'valor',
    'venta_dia': 250000,
    'nota': 4.8,
    'booleano': True
}
print(diccionario)
"""
# Crear diccionario
ventas_semana: dict = {
    'lunes': 10,
    'martes': 12,
    'miercoles': 20,
    'jueves': 25,
    'viernes': 40
}
# Estado inicial del lunes
print(ventas_semana['lunes'])
ventas_semana['lunes'] = 24
# Estado final del diccionario (dia lunes)
print(ventas_semana['lunes'])

# Añadir día sábado
ventas_semana['sabado'] = 60
print(ventas_semana)
print("-------------------------")
# Elimitar un item con su respectiva llave
ventas_semana.pop('lunes')
# Con .values() obtenemos los valores
print(ventas_semana.values())
# keys() obtenemos las llaves
print(ventas_semana.keys())

from os import system
system("clear")

# ------------------DICCIONARIO DE DICCIONARIOS--------
ventas_vendedor: dict = {
    "nombre": "Pepito",
    "apellido": "Perez",
    "cedula": '123456789',
    "ventas": {
        "mes": {
            "dias": {
                "lunes": 10,
                "martes": 45,
                "miercoles": 100
            }
        }
    },
    "promedios": [100, 24.6, 90]
}
#Visualizar el diccionario
print(ventas_vendedor)
print("---------------")
#Entrar a un diccionario hijo
hijo = ventas_vendedor["ventas"]["mes"]["dias"]
print(hijo)
print("---------------")
#Entrar a la llave promedios
lista_promedios = ventas_vendedor["promedios"]
lista_promedios[0] = 200
lista_promedios[1] = 30
print(lista_promedios)
