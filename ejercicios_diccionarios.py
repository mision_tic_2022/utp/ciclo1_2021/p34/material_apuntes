
"""
Ejercicio:
Desarrollar una función que se implementará en un cajero,
esta función será la encargada de procesar los
retiros de dinero.
Reciba como parámetros un diccionario con
la cantidad a retirar y
el saldo inicial del usuario.
Si la cantidad a retirar es superior a $20.000
el cajero cobrará una comisión de $2.000.
Si el monto supera el saldo inicial muestre en pantalla
"El valor a retirar supera el saldo"
En caso de realizar el retiro de forma exitosa,
mostrar el saldo que al cliente le queda.
"""
# Ricardo
"""
def Cajero(saldoInicial: float, CantidadRetiro: float):
    diccionario = {
        "Saldo_Inicial": saldoInicial,
        "Saldo_Retirado": CantidadRetiro
    }

    if CantidadRetiro > 20000:
        comision = CantidadRetiro + 2000
    if CantidadRetiro > saldoInicial:
        print("EL monto a retirar supera la cantidad de saldo en cuenta")
    if CantidadRetiro <= saldoInicial:
        print(
            f"EL retiro ha sido exitoso su saldo nuevo saldo es de: {saldoInicial - CantidadRetiro}")


Cajero(150000, 150000)
"""
"""
def proce_retiros(retiro:int,saldo_in:int):

    saldo_fin:int=""

    if 20000<=retiro:
        saldo_fin=saldo_in-(retiro+2000)
    else:
        saldo_fin=saldo_in-retiro

    if retiro+2000>=saldo_in:
        return "El valor a retirar supera el saldo"
    else:
        return f"Su saldo es {saldo_fin}"

proce_retiros(20000,100000)
"""
def cajero():
    retiro = int(input("Digite el valor a retirar: " ))
    saldo_inicial = int(input("Digite el saldo inicial: " ))

    if retiro >= 20000:
        retiro+=2000

    saldo_actual = saldo_inicial-retiro

    if retiro > saldo_inicial:
        print("El valor a retirar supera el saldo")
    else:
        print(f"El saldo actual es: {saldo_actual} pesos")

cajero()
