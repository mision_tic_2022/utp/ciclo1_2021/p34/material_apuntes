


menor_a_5 = lambda numero: numero<5
numeros: list = [1,2,3,4,5,6,7,8,9]
#Filter-> aplica un función que retorna un booleano sobre una lista de elementos
print( list(filter(menor_a_5, numeros)) )

print("************REDUCE*************")
#Reduce -> retorna un solo valor
from functools import reduce

numeros: list = [1,2,3,4,5,6,7,8,9]

acumulador = 0
for elemento in numeros:
    acumulador += elemento

def suma(acumulador=0,elemento=0):
    return acumulador+elemento

print( reduce(suma, numeros) )

"""
LINK DEL CÓDIGO:
https://paste.ofcode.org/ZWJFnmzgij4LhXQY9mmNWc
"""
