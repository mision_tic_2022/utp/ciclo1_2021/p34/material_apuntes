
"""
def suma(numero1: int, numero2: int):
    return numero1+numero2

def operacion(funcion):
    result = funcion(5,8)
    print(result)


operacion(suma)
"""
"""Fabrica de operaciones"""
def operaciones(caracter: str):
    funcion = None
    if caracter == '+':
        #Crear función local
        def suma(n1,n2):
            return n1+n2
        #Guarda la referencia de la función
        funcion = suma
    elif caracter == '-':
        def resta(n1,n2):
            return n1-n2
        funcion = resta
    elif caracter == '*':
        def multiplicacion(n1,n2):
            return n1*n2
        funcion = multiplicacion

    return funcion

def resultado(func):
    print(func(10,2))

#Creamos referencia de una funcion
r_func = operaciones('*')
#Enviamos la referencia de la función a otra función
resultado(r_func)

def padre():
    def hijo(n1,n2):
        return (n1+n2)/2
    #....
    func_externa(hijo)

def func_externa(func):
    result = func(10,20)
    print(result)

padre()

"""
LINK DEL CÓDIGO:
https://paste.ofcode.org/nKPYnsLGHxpD9q5SvHctSy
"""