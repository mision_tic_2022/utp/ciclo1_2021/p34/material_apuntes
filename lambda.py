"""
def suma(n1,n2):
    return n1+n2

mi_funcion = lambda n1,n2: n1+n2

print( mi_funcion(5,10) )
"""
"""
Desarrolle un programa que reciba como parámetro una lista
de peliculas y un caracter.
Retorne una nueva lista con las películas que empiezan con 
el caracter que recibe como parámetro
--->>>Desarrollar la solución en una sola linea de código<<<---
"""
#List comprehension
[ x for x in range(0,100) ]
#Lambda -> función anónima
filtro_peliculas = lambda peliculas, caracter: [ peli for peli in peliculas if peli[0] == caracter ]

peliculas1 = ["Mi villano favorito", 'El castigador', "El principe de persia",
            "El paseo 5", "Búsqueda implacable"]

peliculas2 = ["Kung fu panda", "El hombre de acero", "Buscando a nemo",
            "Bella eres"]

peliculas3 = ["Mi pobre angelito", "Mamá quien dispara", "La gran estafa"]

print( filtro_peliculas(peliculas1, 'E') )
print( filtro_peliculas(peliculas2, 'B') )
print( filtro_peliculas(peliculas3, 'M') )

"""
LINK DEL CODIGO:
https://paste.ofcode.org/RJV7vKH4HwYz7m4faxCRsV
"""


"""
Desarrolle un funcion que retorne una lista de valores
en el cual el usuario debe indicar el inicio del valor 
y el final. ejemplo: desde 10 hasta 20-> [10,11,12,13,14...20]
"""
#Jhon Oscar
lista_inicio_final = lambda inicio, final: [x for x in range(inicio, final+1)]

#Laura delgado
funcion_inicio_final = lambda inicio, final: [ x for x in range (inicio,final)]

print (funcion_inicio_final(10,21))