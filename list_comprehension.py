
"""
lista: list = []
for x in range(0, 20):
    lista.append(x)

print(lista)
"""
"""
#Operador ternario
num = 6
num%2 == 0
print( 'Verdadero' if num==5 else 'Falso')
#List comprehension
print( [ x for x in range(0,20) if x%2==0 ])
"""

"""
Clave del diccionario sea una tupla,
el primer elemento sea el autoincrementable y 
el segundo elemento sea autoincrementable^2.
El valor del diccionario debe ser una lista
elementos del 1 al 50 que sean divisibles
por el primer elemento de la tupla.
->del 2 al 50
diccionario = {
    (2, 4): [2,4,6,8,10],
    (3, 9): [3,6,9,12...50],
    (4, 16): [4,8,12,16...] 
    ...(50,50^2): [50]
}
"""

mi_diccionario = {
    (x, x**2): [j for j in range(2,51) if j%x==0]  for x in range(2,51)
}
print(mi_diccionario)

print({ (x, x**2): [ j for j in range(2,20) if j%x==0 ] for x in range(2,20) })


#list comprehension
mi_lista = [ x for x in range(0,10)  ]
print(mi_lista)

#Conversion de lista a conjunto
conjunto = set(mi_lista)
print(conjunto)


