

numeros = [1,2,3,4,5,6,7,8,9]

#Elevaar al cuadrado
def cuadrado(numero: int)->int:
    return numero**2

lista_cuadrada = tuple(map(cuadrado, numeros))
print(lista_cuadrada)

"""
Desarrolle un software que permita eliminar las 
tildes de un caracter.
Ejemplo: á -> a, é -> e,  í -> i, ó -> o
"""

def eliminar_tildes(caracter: str)->str:
    new_caracter = ""
    if caracter == 'á':
        new_caracter = 'a'
    elif caracter == 'é':
        new_caracter = 'e'
    elif caracter == 'í':
        new_caracter = 'i'
    elif caracter == 'ó':
        new_caracter = 'o'
    elif caracter == 'ú':
        new_caracter = 'u'
    else:
        new_caracter = caracter

    return new_caracter

lista_vocales: list = ['á', 'é', 'í', 'ó', 'ú']
#Mapeo de datos
print( list(map(eliminar_tildes, lista_vocales)) )