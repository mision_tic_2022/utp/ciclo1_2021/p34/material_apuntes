
"""
*************ZIP*************
    lista de tuplas
"""
"""
caracteres: list = ['a', 'b', 'c']
numeros: list = [1,2,3,4,5,6]
#Aplicamos zip(creamos una tupla de elementos)
#  y luego convertimos a una lista de tuplas
parejas = list(zip(caracteres, numeros))
print(parejas)
"""

"""
******************ANY - ALL***************
"""

condiciones: list = [1==1, 2==2, True, True]
#any
#Si alguno de los elementos es verdadero entonces retorna True
if any(condiciones):
    print("Por lo menos un elemento es verdadero")
else:
    print("Todos los elementos son falsos")

print("------------ALL-------------")
#all
#Si por lo menos un elemento es falso entonces retorna False
if all(condiciones):
    print("Todos los elementos son verdadero")
else:
    print("Almenos un elemento es falso")

"""
LINK DEL CÓDIGO:
https://paste.ofcode.org/ATWMsmYA6DT37xUC6vRhms
"""


