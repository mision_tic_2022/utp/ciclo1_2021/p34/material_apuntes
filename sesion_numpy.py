


#pip install numpy
#Importamos numpy
import numpy as np
#Importamos libreria del sistema
import sys
#Importamos librería manejadora del tiempo
import time

#import pandas as pd
#lista: list = [1,2,3,4,5,6]
#print(lista)
#Crea una matriz 2x4
#arreglo = np.array([[1,2,3,4], [5,4,6,8]])
#print(arreglo)

#Creamos una lista
lista: list = range(1000)
print( sys.getsizeof(1)*len(lista) )

#Creamos matriz de una sola lista con mil elementos
matriz = np.arange(1000)
#Obtenemos el tamaño en memoria de la matriz por la cantidad de items
print( matriz.size*matriz.itemsize )


"""
TIEMPOS DE EJECUCIÓN
"""
print("\tTIEMPOS DE EJECUCIÓN")
tamanio = 1000000
#Creamos dos listas
lista_1 = range(tamanio)
lista_2 = range(tamanio)
#Obtenemos el tiempo en microsegundos
tiempo_inicio = time.time()
#Genera una lista con la sumatoria de
#los elementos de cada posición de las listas
result = [ x+y for x,y in list(zip(lista_1, lista_2)) ]
#Visualizamos el tiempo de ejecución del proceso
print( (time.time()-tiempo_inicio)*1000)

#Crea dos matrices de 1 fila por un millon de columnas
matriz_1 = np.arange(tamanio)
matriz_2 = np.arange(tamanio)
#Obtenemos el tiempo en microsegundos
tiempo_inicio = time.time()
#Sumatoria de matrices
result_m = matriz_1 + matriz_2
#Visualizamos el tiempo de ejecución del proceso
print( (time.time()-tiempo_inicio)*1000)

#Desempaquetado de tuplas
#tupla = (5,2)
#var1,var2 = tupla

"""
LINK DEL CÓDIGO:
https://paste.ofcode.org/3Q2ueTPgsSctuvyDcrYmhH
"""
